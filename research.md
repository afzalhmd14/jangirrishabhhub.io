---
layout: page
title: Research
heading: Research
permalink: /research/
comments: true
---

<p style="text-align: justify; ">Through my 4 years of undergraduate studies I have been working in conjunction with Robotics Research Centre, IIIT Hyderabad and Department of Electronics and Electrical Engineering, IIT Guwahati as a researcher. Here I have tried to compile my research in the form of this webpage for providing a better understanding of my work and capabilities to the reader. My major contributions and interests are in learning, applying and inventing in the fields of (broadly) robotics, artificial Intelligence, computer vision and internet of things.</p>
Do check out my [DIY Robots page](/robots/) to see some cool robots designed and fabricated by me, apart from my research.  


### <u><a href="/lfD/">Learning from Demonstration via Inverse Reinforcement Learning.</a></u>
<p style="text-align: justify; "> Learning from Demonstration (LfD) is the area of study that talks about teaching complex tasks to agents using expert behaviors given in the form of demonstrations. My work focussed on learning anti-breakage strategies in Monocular SLAM using Inverse Reinforcement Learning as an LfD framework. Programmed an artificially intelligent agent capable of learning distinct behaviors from expert demonstrations by estimating the underlying reward functions using Inverse Reinforcement Learning (Abbeel and Ng, 2000). Wrote a <a href="/2016/07/10/virtual-car-IRL/">blog post</a> on the same and released reproduce-able <a href="https://github.com/jangirrishabh/toyCarIRL">code on Github</a> which gained attention in the machine learning community. Successfully applied the inverse reinforcement learning algorithm in a monocular camera equipped mobile robot setting to estimate the reward function for anti-SLAM breakage behaviors.</p>
<div class="imgcap">
<center><img src="/assets/research/lfdMain.png" width="90%"></center>
<div class="thecap" ><a href="/lfD/">Learning anti-breakage strategies in Monocular SLAM using Inverse Reinforcement Learning.</a></div>
</div>

### <u><a href="/monocular/">Monocular SLAM supported Point Cloud Segregation of Identified objects.</a></u>
<p style="text-align: justify; ">My work at the Robotics Research Lab under Dr. K. Madhava Krishna, was on the Parrot AR.Drone, a quadrocopter with a monocular camera, I successfully achieved the segregation of point cloud of a known object from all of the point cloud data available from visual SLAM system. Worked on applying various monocular SLAM techniques on a Parrot AR-Drone, namely Parallel Tracking and Mapping PTAM, ORB-SLAM and LSD-SLAM, evaluated them on the basis of odometry and reconstruction.</p>
<div class="imgcap">
<center><img src="/assets/research/monoMain.png" width="90%"></center>
<div class="thecap" ><a href="/monocular/">Monocular SLAM supported Point Cloud Segregation of Identified objects.</a></div>
</div>

### <u><a href="/navigation/">Autonomous Indoor Navigtion Robot.</a></u>
<p style="text-align: justify; ">We, a bunch of undergraduate students intrigued by the ideas of mobile robotics, led by our senior Tanmay Shankar decided to build a robot capable of autonomous indoor navigation. He introduced us to a powerful tool, the Robot operating system (ROS). Getting our hands dirty with ROS opened up a whole new horizon in robotics for us, we could now easily implement our ideas. The learning curve was steep I must say, but it was for good. Finally we were ready with the first ever autonomous indoor navigating robot in IIT Guwahati. FAIR's design was inspired by the famous Turtle Bot by Willow Garage. FAIR was fabricated in the Robotics Club, IIT Guwahati. We implemented RTAB map and navigation stack packages available from the open-source ROS community.</p>
<div class="imgcap">
<center><img src="/assets/research/fairMain.png" width="90%"></center>
<div class="thecap" ><a href="/navigation/">Autonomous Indoor Navigtion Robot.</a></div>
</div>

### <u><a href="/avs/">Assistive Vision Simulator.</a></u>
<p style="text-align: justify; ">Our work at the REDx Hyderabad Camp 2015, jointly organized by MIT Media Lab Camera Culture Group and LVPEI. Worked as the Hardware lead, Assistive Vision Simulator and developed a virtual reality environment to test various modalities that enable the blind to navigate effciently. Used audio and haptic feedback to alarm the visually impaired of any static obstacles. Tested our device with physically impaired people from LVPEI and received good feedback.</p>
<div class="imgcap">
<center><img src="/assets/research/avsMain.png" width="90%"></center>
<div class="thecap" ><a href="/avs/">Assistive Vision Simulator.</a></div>
</div>

### <u><a href="/ble/">Bluetooth Low Energy Networks.</a></u>
<p style="text-align: justify; ">As an intern at a startup, Simple Labs, Chennai, just after my freshmen year I was introduced to the world of IoT, wireless data transfer protocols and networks. Specifically working on Bluetooth Low Energy a.k.a. Bluetooth 4.0 for a period of 2 months in a completely different city miles away from my home town both in terms of distance and culture was an enthralling experience. Towards the end of my intern I successfully created a linear network of BLE devices and worked on a Vehicle Identification system based on linear network of ble.</p>
<div class="imgcap">
<center><img src="/assets/research/bleMain.png" width="90%"></center>
<div class="thecap" ><a href="/ble/">Bluetooth Low Energy Networks.</a></div>
</div>