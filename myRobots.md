---
layout: page
title: DIY Robots
heading: DIY Robot Hacks
permalink: /robots/
comments: true
---

> A large part of my day is about designing and fabricating robots, I am interested in all things mechanical and hands on. Whenever I see someone holding a screw-driver or fiddling with any electronic component, I make sure I spent the next few hours brainstorming and reverse engineering with them. I have been a part of numerous Hackathons, MIT REDx 2015 (national level), Let's Make '14, Kickstarter'16 (college level). Have a look at my creations.

<div class="imgcap">
<div align="centre">
<iframe width="500" height="280" src="https://www.youtube.com/embed/lFJ5vdC3sLg?rel=0&amp;controls=1&amp;autoplay=0&amp;loop=1&amp;rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>
<div class="thecap"><b>Gap Climbing Robot.</b> </div>
</div>
<p style="text-align: justify; ">  A robot designed to climb narrow gaps, capable of adjusting according to irregularities in the gap size with the help of a suspension system. Completely fabricated through raw material available at local hardware store. </p>



------
<div class="imgcap">
<div align="centre">
<iframe width="500" height="280" src="https://www.youtube.com/embed/6jhQv5RgbWs?rel=0&amp;controls=1&amp;autoplay=0&amp;loop=1&amp;rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>
<div class="thecap"><b>RC Speed boat.</b> </div>
</div>


<p style="text-align: justify;">  A compact RC boat with a powerful brushless DC motor controlled by a 3-phase Electronic speed controller (ESC), capable of going at high speeds and long time duration with the help of a long-lasting Lithium Polymer battery. The rudder system is controlled via a high torque servo motor.</p>


-----

<div class="imgcap">
<div align="centre">
<iframe width="500" height="280" src="https://www.youtube.com/embed/f1smZz4PPQ4?rel=0&amp;controls=1&amp;autoplay=0&amp;loop=1&amp;rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>
<div class="thecap"><b>Smart Prosthetic Leg.</b> </div>
</div>
<p style="text-align: justify;"> Smart Prosthetics employ various sensors on the prosthetic and use the sensor output data to monitor the health of the prosthetic. This data helps the doctor/patient to determine the <i>wellness</i> of the prosthetic as we have tried to show in this small prototype of a prosthetic leg.</p>


------

<div class="imgcap">
<div align="centre">
<iframe width="500" height="280" src="https://www.youtube.com/embed/KEJ7jOg7ses?rel=0&amp;controls=1&amp;autoplay=0&amp;loop=1&amp;rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>
<div class="thecap"><b>Scorbot Restoration.</b> </div>
</div>
<p style="text-align: justify;">  Restored this 10 year old manipulator, the built in controller was a big old box that was not working, thus replaced with Arduino Mega and Dual H-bridge  motor drivers. The software interface involves ROS.</p>


------


<div class="imgcap">
<div align="centre">
<iframe width="500" height="280" src="https://www.youtube.com/embed/u6JBzw7WFis?rel=0&amp;controls=1&amp;autoplay=0&amp;loop=1&amp;rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>
<div class="thecap"><b>Indoor Navigation robot.</b> </div>
</div>
<p style="text-align: justify;"> A differential drive robot made in the famous turtle bot design,  low level motor commands controlled through Arduino and high level processing done  on the laptop. Eventually goes on to become a chassis for an indoor navigation robotic platform.</p>

------

<div class="imgcap">
<div align="centre">
<iframe width="500" height="270" src="https://www.youtube.com/embed/WlY9SgY_iwo?rel=0&amp;controls=1&amp;autoplay=0&amp;loop=1&amp;rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>
<div class="thecap"><b>Pick and Place robot.</b> </div>
</div>
<p style="text-align: justify;"> A simple 3 DOF manipulator to pick and place light blocks. The linear actuator motion is derived from the circular motion of the motor with a worm gear and a simple gear. </p>

------
<div class="imgcap">
<div align="centre">
<iframe width="270" height="480" src="https://www.youtube.com/embed/kqsQR5D09So?rel=0&amp;controls=1&amp;autoplay=0&amp;loop=1&amp;rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>
<div class="thecap"><b>Maze solver robot.</b> </div>
</div>
<p style="text-align: justify;">  Autonomous line following maze solver using a PID control algorithm. The bot returns to the starting position in minimum distance by recording the turns it had to undergo to reach to the goal.</p>

------
<div class="imgcap">
<div align="centre">
<iframe width="500" height="280" src="https://www.youtube.com/embed/HOo5zXjL3PA?rel=0&amp;controls=1&amp;autoplay=0&amp;loop=1&amp;rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>
<div class="thecap"><b>Arduino controlled CNC machine.</b> </div>
</div>
<p style="text-align: justify;">  A Computer Numberic controller(CNC) made out of scrap material using arduino, DC brushed motors and gears.</p>

------