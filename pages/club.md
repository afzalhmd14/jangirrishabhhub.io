---
layout: page
comments: true
heading:  
permalink: /club/
---

<div class="imgcap2">
<center><img src="/assets/home/club12.jpg" width="100%"></center>
<div class="thecap" ><i>"If you want to go fast, go alone. But if you want to go far, go together."</i></div>
</div>

<p style="text-align: justify; ">One of the best experiences in my undergraduate college life has been my involvement with the Robotics Club, IIT Guwahati. Being a very active member I headed the chair as the <b>Secretary, Robotics Club</b> 2015-16. Its been an awesome journey so far, we were crowned the "Best Club Gymkhana'15" for our work. We work on a multitude of projects throughout the year, sharing ideas, discussing difficulties, implementing them and at the end of the year showcasing them to the college community.</p>

<div class="imgcap2">
<center><img src="/assets/home/club5.jpg" width="100%"></center>
<div class="thecap" >The official club logo designed by me.</div>
</div>

<p style="text-align: justify; ">We are a closely knit team, working towards perfection! Talking of perfection, we came up with this logo, deeply inspired from ROS, these 9 dots represent a grid, the orange ones being the obstacles and the yellow ones represent a path between them, the jist being the problems of motion planning and navigation.
</p>

<p style="text-align: justify; ">Being a community of robotics enthusiasts, we teach each other and learn from each other, by organizing lectures and workshops through out the year. Our knowledge has grown over the years, and we are taking up bigger and better challenges every year. With over 50 dedicated undergraduate and graduate students we are working towards improving the technical knowledge of our college community and the nation as a whole. Currently we are working on autonomous indoor navigation, RFID system, underwater robotics, robotic arms, goal line detection system, movable printer, and many more. 
</p>

<div class="imgcap2">
<center><img src="/assets/home/club3.JPG" width="70%"></center>
<div class="thecap" >Team TechEvinche '15.</div>
</div>

<div class="imgcap2">
<center><img src="/assets/home/club2.jpg" width="70%"></center>
<div class="thecap" >Embedded Systems workshop '15.</div>
</div>

<div class="imgcap">
<div align="centre">
<iframe width="660" height="371" src="https://www.youtube.com/embed/6Kmn5sIHLTY?rel=0&amp;controls=1&amp;autoplay=0&amp;loop=1&amp;rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>
<div class="thecap"><b>The following video is a compilation of some of our interesting projects.</b> </div>
</div>