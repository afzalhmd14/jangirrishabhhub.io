---
layout: post
title: DIY Robots
permalink: /robots/
comments: true
---

> A large part of my day is about designing and fabricating robots, I am interested in all things mechanical and hands on. Whenever I see someone holding a screw-driver or fiddling with any electronic component, I make sure I spent the next few hours brainstorming and reverse engineering with them.

<div class="imgcap">
<div align="centre">
<iframe width="500" height="280" src="https://www.youtube.com/embed/lFJ5vdC3sLg?rel=0&amp;controls=1&amp;autoplay=0&amp;loop=1&amp;rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>
<div class="thecap"><b>Gap Climbing Robot.</b> </div>
</div>
<p style="text-align: justify; ">  A robot designed to climb narrow gaps, capable of adjusting according to irregularities in the gap size with the help of a suspension system. Completely fabricated through raw material available at local hardware store. </p>



------
<div class="imgcap">
<div align="centre">
<iframe width="500" height="280" src="https://www.youtube.com/embed/6jhQv5RgbWs?rel=0&amp;controls=1&amp;autoplay=0&amp;loop=1&amp;rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>
<div class="thecap"><b>RC Speed boat.</b> </div>
</div>


<p style="text-align: justify;">  A compact RC boat with a powerful brushless DC motor controlled by a 3-phase Electronic speed controller (ESC), capable of going at high speeds and long time duration with the help of a powerful Lithium Polymer battery. The rudder system is controlled via a high torque servo motor.</p>


-----

<div class="imgcap">
<div align="centre">
<iframe width="500" height="280" src="https://www.youtube.com/embed/f1smZz4PPQ4?rel=0&amp;controls=1&amp;autoplay=0&amp;loop=1&amp;rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>
<div class="thecap"><b>Smart Prosthetic Leg.</b> </div>
</div>
<p style="text-align: justify;">  A robot designed to climb narrow gaps, capable of adjusting according to irregularities in the gap size with the help of a suspension system. Completely fabricated through raw material available at local hardware store. </p>


------

<div class="imgcap">
<div align="centre">
<iframe width="500" height="280" src="https://www.youtube.com/embed/KEJ7jOg7ses?rel=0&amp;controls=1&amp;autoplay=0&amp;loop=1&amp;rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>
<div class="thecap"><b>Scorbot Restoration.</b> </div>
</div>
<p style="text-align: justify;">  A robot designed to climb narrow gaps, capable of adjusting according to irregularities in the gap size with the help of a suspension system. Completely fabricated through raw material available at local hardware store. </p>


------


<div class="imgcap">
<div align="centre">
<iframe width="500" height="280" src="https://www.youtube.com/embed/u6JBzw7WFis?rel=0&amp;controls=1&amp;autoplay=0&amp;loop=1&amp;rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>
<div class="thecap"><b>Indoor Navigation robot.</b> </div>
</div>
<p style="text-align: justify;">  A robot designed to climb narrow gaps, capable of adjusting according to irregularities in the gap size with the help of a suspension system. Completely fabricated through raw material available at local hardware store. </p>

------

<div class="imgcap">
<div align="centre">
<iframe width="500" height="270" src="https://www.youtube.com/embed/WlY9SgY_iwo?rel=0&amp;controls=1&amp;autoplay=0&amp;loop=1&amp;rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>
<div class="thecap"><b>Pick and Place robot.</b> </div>
</div>
<p style="text-align: justify;">  A robot designed to climb narrow gaps, capable of adjusting according to irregularities in the gap size with the help of a suspension system. Completely fabricated through raw material available at local hardware store. </p>

------
<div class="imgcap">
<div align="centre">
<iframe width="270" height="480" src="https://www.youtube.com/embed/kqsQR5D09So?rel=0&amp;controls=1&amp;autoplay=0&amp;loop=1&amp;rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>
<div class="thecap"><b>Maze solver robot.</b> </div>
</div>
<p style="text-align: justify;">  A robot designed to climb narrow gaps, capable of adjusting according to irregularities in the gap size with the help of a suspension system. Completely fabricated through raw material available at local hardware store. </p>

------
<div class="imgcap">
<div align="centre">
<iframe width="500" height="280" src="https://www.youtube.com/embed/HOo5zXjL3PA?rel=0&amp;controls=1&amp;autoplay=0&amp;loop=1&amp;rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
</div>
<div class="thecap"><b>Arduino controlled CNC machine.</b> </div>
</div>
A robot designed to climb narrow gaps, capable of adjusting according to irregularities in the gap size with the help of a suspension system. Completely fabricated through raw material available at local hardware store.

------